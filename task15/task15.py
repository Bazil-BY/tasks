from functools import reduce
from math import sqrt


def min_sum(arr):
    arr = sorted(arr)
    return sum(arr[i] * arr[-(i+1)] for i in range(len(arr) // 2))


def max_product(lst, n):
    return reduce(lambda x, y: x * y, sorted(lst)[-n:])


def array_leaders(mass):
    return [mass[i] for i in range(len(mass)) if mass[i] > sum(mass[i + 1:])]


def max_gap(mass):
    mass = sorted(mass)
    return max((abs(mass[i] - mass[i + 1]) for i in range(len(mass) - 1)))


def product_array(mass):
    return [reduce(lambda x, y: x * y, mass[:i] + mass[i+1:]) for i in range(len(mass))]


def max_tri_sum(mass):
    return sum(sorted(set(mass))[-3:])


def row_weights(mass):
    return sum(mass[::2]), sum(mass[1::2])


def min_value(mass):
    return int(''.join((str(el) for el in sorted(set(mass)))))


def minimum_number(mass):
    x = sum(mass)
    prime = False
    while not prime:
        prime = True
        for i in range(2, int(sqrt(x))+1):
            if x % i == 0:
                prime = False
                x += 1
                break
    return x - sum(mass)


def adjacent_element_product(array):
    return max((array[i] * array[i+1] for i in range(len(array)-1)))


def nth_smallest(arr, pos):
    return sorted(arr)[pos-1]
