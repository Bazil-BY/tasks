import itertools


def find_last(n, m):
    lst = [[i, 0] for i in range(1, n + 1)]
    while len(lst) > 1:
        i = 0
        tmp = []
        len_lst = len(lst)
        for el in itertools.cycle(lst):
            i += 1
            index = i % len_lst - 1
            if index == -1: index = len_lst - 1

            if i <= len_lst:
                tmp.append(el)

            if i <= m:
                if len_lst < m:
                    tmp[index][1] += 1
                else:
                    tmp[i - 1][1] += 1

            if i == m + 1:
                if len_lst < m + 1:
                    tmp[index][1] = tmp[index][1] + tmp[index - 1][1]
                else:
                    tmp[i - 1][1] = tmp[i - 1][1] + tmp[i - 2][1]

            if m + 1 <= i <= len_lst:
                tmp[i - 1][1] += 2

            if m < len_lst == i or len_lst <= m == i-1:
                if len_lst >= m:
                    index = m

                if index != 0:
                    tmp.extend(tmp[:index])
                    lst = tmp[index:]
                else:
                    lst = tmp

                lst.pop()
                break
    return lst[0]


n = int(input('n: '))
m = int(input('m: '))
print(find_last(n, m))
