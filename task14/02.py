from math import factorial, sqrt


def balanced_num(number):
    n = str(number)
    middle = len(n) // 2

    if len(n) % 2 == 0:
        delta = -1
    else:
        delta = 0

    n1 = [int(el) for el in n[:middle + delta]]
    n2 = [int(el) for el in n[middle + 1:]]

    if sum(n1) == sum(n2):
        balance = "Balanced"
    else:
        balance = "Not Balanced"

    return balance


def strong_num(number):
    return "Strong !!!" if number == sum([factorial(int(el)) for el in str(number)]) else "Not Strong !!!"


def disarium_number(number):
    return "Disarium !!" if number == sum([int(el)**(index+1) for index, el in enumerate(str(number))]) else "Not !!"


def jumping_number(number):
    j = True
    n = str(number)
    d = len(n)
    if d != 1:
        index = 0
        while j and index < d-1:
            index += 1
            j = abs(int(n[index]) - int(n[index-1])) == 1
    return "Jumping!!" if j else "Not!!"


def special_number(number):
    return "Special!!" if set(int(el) for el in str(number)).issubset(set(range(6))) else "NOT!!"


def automorphic(n):
    number = str(n**2)[-len(str(n)):]
    return "Automorphic" if number == str(n) else "Not!!"


def extra_perfect(n):
    result = []
    for number in range(1, n+1):
        if bin(number)[2:][0] == bin(number)[2:][-1] == '1':
            result.append(number)
    return result


def tidyNumber(number):
    j = True
    n = str(number)
    if len(n) != 1:
        index = 0
        while j and index < len(n)-1:
            index += 1
            j = int(n[index]) >= int(n[index-1])
    return j


def num_primorial(n):
    result = 2
    m = 1
    x = 3
    while m < n:
        simple = True
        for i in range(2, int(sqrt(x))+1):
            if x % i == 0:
                simple = False
                break
        if simple:
            m += 1
            result = result * x
        x += 1

    return result


def sum_alt(n1: int, n2: int):
    if n2 == 0: return n1
    xor_bin = n1 ^ n2
    and_corr = (n1 & n2) << 1

    print('n1',bin(n1)[2:].zfill(8))
    print('n2', bin(n2)[2:].zfill(8))
    print(' ^', bin(xor_bin)[2:].zfill(8))
    print(' &', bin(n1 & n2)[2:].zfill(8))
    print(' <', bin(and_corr)[2:].zfill(8))

    return sum_alt(xor_bin, and_corr)


r = sum_alt(75, 67)
print(r, bin(r)[2:].zfill(8))
