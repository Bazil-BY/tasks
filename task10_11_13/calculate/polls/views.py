from django.urls import reverse
from django.views import generic
from django.shortcuts import render, get_object_or_404
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect

from .models import Choice, Question

__all__ = {'IndexView', 'DetailView', 'ResultsView', 'vote'}


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request: HttpRequest, question_id: int) -> HttpResponse:
    question = get_object_or_404(Question, pk=question_id)
    if 'choice' not in request.POST:
        return render(request, 'polls/detail.html', dict(
            question=question,
            error_message="You aren't set a choice",
        ))

    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except Choice.DoesNotExist:
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', dict(
            question=question,
            error_message="Undefined choice",
        ))

    selected_choice.votes += 1
    selected_choice.save()
    return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
