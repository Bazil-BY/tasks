from django.urls import path

from . import views

app_name = 'calc'
urlpatterns = [
    path('', views.MainView.as_view(), name='main'),
    path('calc/', views.IndexView.as_view(), name='index'),
    path('calc/api/', views.ApiView.as_view(), name='api'),
    path('calc/history/', views.HistoryView.as_view(), name='history'),
]
