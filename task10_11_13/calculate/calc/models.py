from django.db import models


class Operator(models.Model):
    operator = models.CharField(max_length=10)
    operator_info = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.operator


class Operation(models.Model):
    operator = models.ForeignKey(Operator, on_delete=models.CASCADE)
    arg1 = models.FloatField()
    arg2 = models.FloatField()
    operation_text = models.CharField(max_length=200, default='')
    pub_date = models.DateTimeField('date published')
    result = models.FloatField(default=0)

    def __str__(self):
        return self.operation_text
