def calc(operator, x, y: str) -> str:
    result = ''
    x = float(x.strip('()'))
    y = float(y.strip('()'))

    if operator == '**':
        result = x ** y
    elif operator == '*':
        result = x * y
    elif operator == '/':
        result = x / y
    elif operator == '+':
        if x == '': x = int('0')
        result = x + y
    elif operator == '-':
        if x == '': x = int('0')
        result = x - y

    return str(result)
