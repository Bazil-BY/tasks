from django.contrib import admin

from .models import Operation, Operator

admin.site.register(Operation)
admin.site.register(Operator)
