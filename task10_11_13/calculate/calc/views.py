from django.utils import timezone
from django.views import generic
from django.http import JsonResponse, HttpResponse, QueryDict
from .models import Operator, Operation
from django.shortcuts import get_object_or_404, render

from .mat_calc import calc

__all__ = {'MainView', 'IndexView', 'HistoryView'}

OPERATORS = {'add': '+', 'sub': '-', 'mul': '*', 'div': '/'}
API_KEYS = ['arg1', 'arg2', 'act', 'add', 'sub', 'mul', 'div', 'history']


def calculate(post: QueryDict):
    rslt = Operation(
            operator=get_object_or_404(Operator, operator=post['act']),
            arg1=post['arg1'],
            arg2=post['arg2'],
            pub_date=timezone.now(),
            )
    try:
        rslt.result = calc(str(rslt.operator), rslt.arg1, rslt.arg2)
    except Exception as e:
        rslt = e
    else:
        rslt.operation_text = f'{rslt.arg1 if float(rslt.arg1) >= 0 else "(" + rslt.arg1 + ")"} ' \
                              f'{rslt.operator} ' \
                              f'{rslt.arg2 if float(rslt.arg2) >= 0 else "(" + rslt.arg2 + ")"} ' \
                              f'= {rslt.result}'
        rslt.save()
    return rslt


class MainView(generic.View):
    template_name = 'calc/main.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class ApiView(generic.View):

    def get(self, request):
        result = request.GET.copy()
        if len(result.keys()) and set(result.keys()).issubset(API_KEYS):
            if {'act', 'arg1', 'arg2'}.issubset(result.keys()) and result['act'] in OPERATORS.keys():
                result.update({'act': OPERATORS[result['act']]})
                el = calculate(result)
                result = request.GET.copy()
                if type(el) is Operation:
                    result.update({'result': el.result})
                elif isinstance(el, Exception):
                    result.update({'result': str(el), 'error': type(el).__name__})
                return JsonResponse(result)
            if 'history' in result.keys():
                result.update({'result': [el.operation_text
                                          for el in Operation.objects.order_by('-pub_date')[:int(result['history'])]]})
                return JsonResponse(result)

        return HttpResponse(f'<h3>Empty or wrong API request.</h3><br>' 
                            f'For example,<br>' 
                            f'<i>?arg1=45&arg2=9&act=div<br>'
                            f'?history=5<br><br></i>'
                            f'For errors,<br>'
                            f'<i>?arg1=5&arg2=<b>0</b>&act=div</i> - division by zero<br>'
                            f'<i>?arg1=5&arg2=<b>2s</b>&act=div</i> - invalid argument')


class IndexView(generic.ListView):
    template_name = 'calc/index.html'

    def get(self, request, *args, **kwargs):
        operator_list = Operator.objects.all()
        context = {'operator_list': operator_list}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = {}
        try:
            operator_curr = get_object_or_404(
                Operator,
                pk=request.POST['operator_selected']
            )
        except (KeyError, Operator.DoesNotExist):
            return render(
                request,
                'calc/index.html',
                {'error_message': "You didn't select a operator or enter operand"}
            )
        else:
            r = request.POST.copy()
            r.update({'act': str(operator_curr)})
            context.update({'arg1': r['arg1'], 'arg2': r['arg2'],
                            'selected_id': int(r['operator_selected'])})
            el = calculate(r)
            if type(el) is Operation:
                context.update({'result': el.result})
            elif isinstance(el, Exception):
                context.update({'result': str(el), 'error': type(el).__name__})

        operator_list = Operator.objects.all()
        context.update({'operator_list': operator_list})
        return render(request, self.template_name, context)


class HistoryView(generic.ListView):
    template_name = 'calc/history.html'
    context_object_name = 'operation_list'

    def get_queryset(self):
        return Operation.objects.order_by('-pub_date')[:10]
