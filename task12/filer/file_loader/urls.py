from django.urls import path

from . import views

app_name = 'fileloader'
urlpatterns = [
    path('fileloader/', views.IndexView.as_view(), name='index'),
    path('fileloader/results/<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('fileloader/results/', views.ResultsView.as_view(), name='results'),
]
