from django.db import models
from jsonfield import JSONField


class FileItem(models.Model):
    title = models.CharField(max_length=50)
    json_data = JSONField()

    def __str__(self):
        return self.title
