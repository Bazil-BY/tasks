import json
from django.urls import reverse
from django.shortcuts import render
from django.utils.datastructures import MultiValueDictKeyError
from django.views import generic
from .models import FileItem
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse, HttpResponse, QueryDict, HttpResponseRedirect


SIGN_DEL = ['"', '(', ')', '-', '?', "'", '\\n']

__all__ = {'IndexView', 'DetailView', 'ResultsView'}


class IndexView(generic.ListView):
    template_name = 'fileloader/index.html'

    def get(self, request, *args, **kwargs):
        # operator_list = Operator.objects.all()
        # context = {'operator_list': operator_list}
        context = {'error': False}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = {}
        try:
            f = request.FILES['file_path']
            obj, created = FileItem.objects.get_or_create(title=f.name)
            if not created:
                raise Exception('Файл уже загружен. Повторите выбор.')
            if f.content_type != 'text/plain':
                raise Exception('Файл не текстовый. Повторите выбор.')
            file = f.read()
        except MultiValueDictKeyError:
            return render(request, 'fileloader/index.html',
                          {'error': 'Файл не загружен. Повторите выбор.'})
        except Exception as e:
            return render(request, 'fileloader/index.html', {'error': e})
        else:
            file = str(file)
            for sign in SIGN_DEL:
                file = file.replace(sign, ' ')
            file = file.split()
            result = {}
            for word in file:
                result[word] = result.setdefault(word, 0) + 1
            obj.json_data = json.dumps(result)
            obj.save()
            return HttpResponseRedirect(reverse('fileloader:results'))


class DetailView(generic.ListView):
    model = FileItem
    template_name = 'fileloader/detail.html'

    def get(self, request, *args, **kwargs):
        file = FileItem.objects.get(id=kwargs['pk'])
        words_list = {key: str(value) for key, value in json.loads(file.json_data).items()}
        context = {'file_title': file.title, 'words_list': words_list}
        return render(request, self.template_name, context)


class ResultsView(generic.ListView):
    template_name = 'fileloader/results.html'
    context_object_name = 'files_list'

    def get_queryset(self):
        return FileItem.objects.all()
